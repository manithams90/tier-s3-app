FROM python:alpine3.10
COPY app.py requirements.txt /app/
WORKDIR /app
RUN pip3 install -r requirements.txt
EXPOSE 5000
CMD python3 ./app.py
