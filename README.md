# tier-s3-app
This repo contains the relevant files to deploy the tier-s3-app to a minikube environment.

* `app.py` - python application which enumerate the objects in s3 bucket
* `deploy.sh` - A simple bash script which deploy the application to minikube (As a parameter we have to pass the namespace)
eg:
  ```
    ./deploy.sh dev
  ```
* `Dockerfile` -  Dickerfile to build the application.
* `requirements.txt` - Required dependencies to build the python appication.

## Infra

All the infastructure related code is stored in the `/infra` directory.
There is a terraform module to deploy a s3 bucket with a S3 bucket policy. `s3_module` directory.
* `blob-store-s3` terraform module creats the s3 bucket in the AWS

## helm

This directory contains all the helm related configurations.

## Manifets

Configuration files related to required kubernetes objects.

## Possible CI/CD setup

![Possible CI/CD sketch](docs/tier-ci-cd.png)

## Credential storage

The credentials can be stored in a external secret engine (eg : hashicorp vault).
During the pipeline we can query the vault API and get the required values to generate the kubernetes secrets object.
