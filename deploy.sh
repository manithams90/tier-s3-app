#!/bin/sh

set -e

if [ -z $1 ]
then
  echo "*** Please define a namespace ***"
  echo "Example: ./deploy.sh dev"
  exit 1
elif [ -n $1 ]
then
# Make first arg as a namespace
  NAMESPACE=$1
  echo "Namespace="$NAMESPACE
fi

timestamp=$(date +%Y%m%d%H%M%S)
docker build -t manithams/tier-s3-app:$timestamp .
docker push manithams/tier-s3-app:$timestamp

helm upgrade -i -f helm/tier-s3-app/values-dev.yaml --set image.tag=$timestamp tier-s3-app-$NAMESPACE helm/tier-s3-app --namespace $NAMESPACE