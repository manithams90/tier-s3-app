provider "aws" {
  region = "eu-central-1"
}

module "blob-store-s3" {
  source = "../s3_module"

  environment  = "dev"
  usage        = "blobs-store"
  project      = "devops"
  whitelist_ip = "18.194.220.217/32"
}