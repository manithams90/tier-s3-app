locals {
  # Common tags to be assigned to all resources
  default_tags = {
    team    = "devops"
    use     = "infra"
    env     = var.environment
    managed = "terraform"
  }
}