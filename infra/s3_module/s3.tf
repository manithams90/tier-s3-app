resource "aws_s3_bucket" "s3_bucket" {
  bucket = format("%s-%s-%s-bucket", var.usage, var.project, var.environment)
  acl    = "private"
  tags = merge(
    {
      "Name" = format("%s-%s-%s-bucket", var.usage, var.project, var.environment)
    },
    local.default_tags
  )
  versioning {
    enabled = true
  }
}

resource "aws_s3_bucket_policy" "s3_bucket_policy" {
  bucket = aws_s3_bucket.s3_bucket.id
  policy = jsonencode({
    Version = "2012-10-17"
    Id      = "s3_bucket_policy"
    Statement = [
      {
        Sid       = "IPAllow"
        Effect    = "Deny"
        Principal = "*"
        Action    = "s3:*"
        Resource = [
          aws_s3_bucket.s3_bucket.arn,
          "${aws_s3_bucket.s3_bucket.arn}/*",
        ]
        Condition = {
          NotIpAddress = {
            "aws:SourceIp" = var.whitelist_ip
          }
        }
      },
    ]
  })
}