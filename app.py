import boto3
import os
from json2html import *
from bottle import route, request, response, template
from bottle import run

BUCKET_NAME = "blobs-store-devops-dev-bucket"  # Configure bucket name


@route('/list', method='GET')
def get_objs():
    client = boto3.client('s3',
                          aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'),
                          aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY'))
    response = client.list_objects(Bucket=BUCKET_NAME)
    r = template("{{!data}}", data=json2html.convert(json=response))
    return(r)


if __name__ == '__main__':
    run(host="0.0.0.0", port=5000, debug=True)
